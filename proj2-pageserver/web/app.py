from flask import Flask
from flask import render_template, abort
import re
import os

app = Flask(__name__)

@app.route("/<path:path>")
def working(path):
    compath = str(path)
    if compath == "/":
        return "UOCIS docker demo!"
    elif re.search("//|~|\.\.", compath) != None:
        return abort(403)
    elif os.path.exists("templates/" + path):
        return render_template(path)
    elif re.search(".html$|.css$", compath) != None:
        return abort(404)
    else:
        return "something went wrong!"

@app.errorhandler(404)
def error_404(erorr):
    return render_template("404.html"), 404 #data, 404

@app.errorhandler(403)
def error_403(erorr):
    return render_template("403.html"), 403


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
